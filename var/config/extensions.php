<?php 

return [
    "bundle" => [
        "Pimcore\\Bundle\\ObjectMergerBundle\\ObjectMergerBundle" => TRUE,
        "Pimcore\\Bundle\\NumberSequenceGeneratorBundle\\NumberSequenceGeneratorBundle" => FALSE,
        "CustomerManagementFrameworkBundle\\PimcoreCustomerManagementFrameworkBundle" => TRUE,
        "Pimcore\\Bundle\\EcommerceFrameworkBundle\\PimcoreEcommerceFrameworkBundle" => TRUE
    ]
];
